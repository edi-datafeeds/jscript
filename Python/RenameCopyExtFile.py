
import os
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("arg1")
parser.add_argument("arg2")
parser.add_argument("arg3")
parser.add_argument("arg4")

args = parser.parse_args()

Fpath = args.arg1
for file in next(os.walk(Fpath))[2]:
    if file.endswith(args.arg2):
        os.rename(os.path.join(Fpath, file), os.path.join(Fpath, args.arg3, args.arg4))
    break